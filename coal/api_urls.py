from django.urls import include, path
from rest_framework.routers import SimpleRouter

from coal.viewsets import CoalViewSet


router = SimpleRouter()
router.register(prefix='coals', viewset=CoalViewSet, basename='coal')

urlpatterns = [path(route='', view=include(arg=router.urls))]
