from django.contrib.admin import ModelAdmin, register

from coal.models import Coal


@register(Coal)
class CoalAdmin(ModelAdmin):
    list_display = ('price', 'grade_coal')
    list_editable = ('grade_coal',)
    list_filter = ('grade_coal',)
