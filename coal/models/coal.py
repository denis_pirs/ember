from django.db import models
from django.db.models import CharField

from coal.enums import CoalType


class Coal(models.Model):
    grade_coal = CharField(verbose_name='Марка угля', max_length=255, choices=CoalType.choices)

    price = CharField(verbose_name='Цена за тонну', max_length=255)

    class Meta:
        verbose_name = 'Цена за тонну'
        verbose_name_plural = 'Цена за тонну'

    def __str__(self) -> str:
        return f'{self.grade_coal} {self.price}'
