from rest_framework.serializers import ModelSerializer

from coal.models import Coal


class CoalSerializer(ModelSerializer):

    class Meta:
        model = Coal
        fields = ('id', 'grade_coal', 'price')
