from django.db.models import TextChoices


class CoalType(TextChoices):
    DMSH = "ДМСШ 0-25"
    DO = "ДО 25-50"
    DPK = "ДПК 50-200"
    DR = "ДР 0-300"
    ROW = "Рядовка"
    FORTIFIED = "Обогащённый"
    BREED = "Порода"
