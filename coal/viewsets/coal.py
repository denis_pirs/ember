from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet

from coal.models import Coal
from coal.serializers import CoalSerializer


class CoalViewSet(ListModelMixin, GenericViewSet):
    queryset = Coal.objects.all()
    serializer_class = CoalSerializer
