from django.urls import include, path
from django.urls import URLResolver


app_name = 'api'

urlpatterns: list[URLResolver] = [
    path(route='', view=include(arg='order.api_urls')),
    path(route='', view=include(arg='coal.api_urls')),
]
