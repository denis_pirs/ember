from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path(route='', view=include(arg='website.urls', namespace='website')),
    path(route='admin/', view=admin.site.urls),
    path(route='api/', view=include(arg='Ember.api_urls', namespace='api')),
]
