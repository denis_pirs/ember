from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet

from order.models import Order
from order.serializers import OrderSerializer


class OrderViewSet(ListModelMixin, GenericViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
