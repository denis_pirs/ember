from rest_framework import status
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, DestroyModelMixin, CreateModelMixin, UpdateModelMixin
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from order.models import Order
from coal.models import Coal
from order.serializers import OrderSerializer
from coal.serializers import CoalSerializer

from random import randint


class TestsHttpViewSet(ModelViewSet):
    queryset = Coal.objects.all()
    serializer_class = CoalSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if not request.user.is_authenticated:
            instance.price += 1000

        serializer = self.get_serializer(instance)
        return Response(serializer.data)

