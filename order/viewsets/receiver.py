from rest_framework import status
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from order.models import Order
from coal.models import Coal

from random import randint


class ReceiverAPIView(APIView):

    def post(self, request: Request) -> Response:
        application = request.data

        address = application.get('address_to', '')
        amount_coal = application.get('amount_coal', '')
        types_coal = application.get('types_coal', '')
        email = application.get('email', '')

        price_km = 10
        coal = Coal.objects.get(grade_coal=types_coal)

        my_address = randint(1, 100)
        price_order = (price_km * my_address) + (int(coal.price) / 1000 * amount_coal)

        email_code = randint(1000, 9999)
        order = Order(price_total=price_order, address_to=address, email=email, types_coal=coal, code=email_code)
        order.save()

        return Response(data={'price': order.price_total, 'id': order.id}, status=status.HTTP_201_CREATED)
