from rest_framework import status
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from order.models import Order


class ApprovedAPIView(APIView):

    def post(self, request: Request) -> Response:
        application = request.data

        mail_code = application.get('code', '')
        agreed = application.get('is_agreed', '')
        id_order = application.get('id', '')

        order = Order.objects.get(id=id_order)
        code = order.code

        if agreed is False:
            return Response(data={'code': 'Извините, но вам необходимо согласиться с обработкой персональных данных'},
                            status=status.HTTP_400_BAD_REQUEST)

        if mail_code != code:
            return Response(data={'code': 'Извините, не верный код'}, status=status.HTTP_400_BAD_REQUEST)

        order.is_approved = True
        order.is_agreed = True
        order.save()
        return Response(data={'code': 'Ваш заказ оформлен, ожидайте'}, status=status.HTTP_201_CREATED)
