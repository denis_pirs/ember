from django.db import models
from django.db.models import CharField, DateTimeField, EmailField, ForeignKey, IntegerField, BooleanField
from django.utils.timezone import now

from coal.models import Coal


class Order(models.Model):
    date_order = DateTimeField(verbose_name='Дата заказа', default=now)

    price_total = CharField(verbose_name='Цена за необходимое колличество', max_length=255)

    address_to = CharField(verbose_name='Адрес заказчика', max_length=255)

    email = EmailField(verbose_name='Почта', max_length=254)

    types_coal = ForeignKey(Coal, verbose_name='Типы угля', on_delete=models.CASCADE)

    code = IntegerField(verbose_name='Код подтверждения', null=True)

    is_approved = BooleanField(verbose_name='Подтверждение заказа', default=False)

    is_agreed = BooleanField(verbose_name='Согласие с политикой конфиденциальности', default=False)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self) -> str:
        return f'{self.address_to} {self.price_total}'
