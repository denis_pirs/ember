from rest_framework.serializers import ModelSerializer

from order.models import Order


class OrderSerializer(ModelSerializer):

    class Meta:
        model = Order
        fields = ('date_order', 'price_total', 'address_to', 'email', 'types_coal', 'code')
