from django.urls import include, path
from rest_framework.routers import SimpleRouter

from order.viewsets import ApprovedAPIView, OrderViewSet, ReceiverAPIView, TestsHttpViewSet


router = SimpleRouter()
router.register(prefix='orders', viewset=OrderViewSet, basename='order')
router.register(prefix='tests_http', viewset=TestsHttpViewSet, basename='tests_http')

urlpatterns = [path(route='', view=include(arg=router.urls)),
               path(route='receiver/', view=ReceiverAPIView.as_view(), name='receiver'),
               path(route='approved/', view=ApprovedAPIView.as_view(), name='approved'),]
