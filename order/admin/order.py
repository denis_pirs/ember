from django.contrib.admin import ModelAdmin, register

from order.models import Order


@register(Order)
class OrderAdmin(ModelAdmin):
    list_display = ('price_total', 'address_to',)
    list_editable = ('address_to',)
